package map.object;

import h2d.Bitmap;
import h2d.Tile;
import h2d.Object;
import motion.Actuate;
import motion.easing.Quad;
import motion.easing.Linear;
import pathfinder.Pathfinder;
import pathfinder.Coordinate;
import pathfinder.EHeuristic;
import map.pathfinder.AstarMap;
import scene.AdvMap;

/**
 * 
 * currently it's a single bitmap but in the futur it will be a Anim
 */

class Hero extends Bitmap { // currently bitmap but will be anim
	private var maxMovePoint:Float;
	private var movePoint:Float;
	private var isOnBoat:Bool;
	private var attack:Int;
	private var defense:Int;
	private var fov:Float;
	private var mind:Int; // define max mana
	private var faith:Int; // define is the hero cast dark or light spell (  you have poison a dark magic spell but if you have positive faith it become heal)
	private var speed:Int; // add to the speed of creature on the battlefield
	private var tx:Int;
	private var ty:Int;
	private var moral:Int;
	private var description:String;
	private var heroName:String;
	 private var team:Team;
	 //private var faction:Faction;

/**
 * create a hero that can move on the tilemap using the pathfinding algorythme
 * @param name the name of the hero
 * @param desc the description of the hero
 * @param tx x position in tile on the tilemap of the hero
 * @param ty y position in tile on the tilemap of the hero
 * @param sprite sprite displayed on the tilemap
 * @param parent parent object
 */

public function new(team:Team, name:String, desc:String, tx:Int, ty:Int, sprite:Tile, parent:Object) {

	 super(sprite, parent);
	 this.team = team;	 
	 this.heroName = name;
	 this.description = desc;
	 this.tx = tx;
	 this.ty = ty;
	 this.team.heroList.push(this);
	 updatePosition();

}
/**
 * get the x position in tile of the hero
 * @return Int
 */
public function getTx():Int {
 return this.tx;
}
/**
 * get the y position in tile of the hero
 * @return Int
 */
public function getTy():Int {
	 return this.ty;
}
/**
 * set the x position in tile of the hero ( don't forget to update the position of the sprite with update position)
 * @param tx x position in tile
 */
public function setTx(tx:Int):Void {
	this.tx = tx;
}
/**
 * set the y position in tile of the hero ( don't forget to update the position of the sprite with update position)
 * @param ty y position in tile
 */
 
public function setTy(ty:Int):Void {
	this.ty = ty;
}
/**
 * set the x and y position in tile of the hero ( don't forget to update the position of the sprite with update position)
 * @param tx x position in tile
 * @param ty y position in tile
 */
public function setTilePosition(tx:Int, ty:Int):Void {

	this.ty = ty;
	this.tx = tx;



}
/**
 * convert the position in tile to position in pixel on the layers
 * @param time time in sec for the transition from the last last position to the new position
 * @param easing easing use for the transition
 */
public function updatePosition(time:Float=0,?easing:motion.easing.IEasing):Void {
	 var currentEasing:motion.easing.IEasing;
	 if (easing == null) {
			currentEasing = Linear.easeNone;
	 } else {
			currentEasing = easing;
	 }
	 
	 Actuate.tween (this, time, { x: (this.tx - this.ty) * 128/2 }).ease (currentEasing);
	 Actuate.tween (this, time, { y: (this.tx + this.ty) * 64/2  }).ease (currentEasing);
	

}

/**
 * calculate the shortest path beetween 2 tile of the tilemap
 * ( currently no parameters in the futur you will need to starting tile position and finish tile position meaby )
 */
public function calcPath():Void {

	 var l_map = new AstarMap( scene.AdvMap.mw, scene.AdvMap.mh ); // Create a 30x30 map
	 var l_pathfinder = new Pathfinder( l_map ); // Create a Pathfinder engine configured for our map
	 var l_startNode = new Coordinate( scene.AdvMap.lastSelectedTile.tx , scene.AdvMap.lastSelectedTile.ty ); // 	The starting node
	 var l_destinationNode = new Coordinate( scene.AdvMap.currentSelectedTile.tx, scene.AdvMap.currentSelectedTile.ty ); // The destination node
	 var l_heuristicType = EHeuristic.PRODUCT; // The method of A Star used
	 var l_isDiagonalEnabled = true; // Set to false to ensure only up, left, down, right movements are allowed
	 var l_isMapDynamic = false; // Set to true to force fresh lookups from IMap.isWalkable() for each node's isWalkable property (e.g. for a dynamically changing map)
	
	 //trace(l_map.isWalkable(scene.AdvMap.hero.tx, scene.AdvMap.hero.ty));
	 if ( !(l_startNode.isEqualTo(l_destinationNode) && l_map.isWalkable(l_destinationNode.x, l_destinationNode.y) ) ) {
			var l_path = l_pathfinder.createPath( l_startNode, l_destinationNode, l_heuristicType, l_isDiagonalEnabled, l_isMapDynamic );
			var j = 0;
			l_path.shift();
			if (l_path != null) {
				 for ( i in l_path )
				 {  
						
						if (j == 0) {
							 setTilePosition(i.x, i.y);
							 updatePosition(0.6, Quad.easeIn);
							 trace(this.tx + " " + this.ty);
						} else if ( j == (l_path.length - 1) ) {
							 Actuate.timer ((j-1)*0.2+0.6).onComplete (setTilePosition, [i.x, i.y]);
							 Actuate.timer ((j-1)*0.2+0.6).onComplete (updatePosition, [0.8, Quad.easeOut]);
						} else {
							 Actuate.timer ((j-1)*0.2+0.6).onComplete (setTilePosition, [i.x, i.y]);
							 Actuate.timer ((j-1)*0.2+0.6).onComplete (updatePosition, [0.2]);
						}
						j++;
						
							 
						// handle the path - e.g. have an NPC add each coodinate as a waypoint to process
				 }
			}
	 } 
}
override public function toString():String {

	 return this.heroName + " " + this.description + " " + tx +" x, " + ty +" y";

}

	 
}
 
 