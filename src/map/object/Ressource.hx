package map.object;

class Ressource {
    public static var gold:Int = 0;
    public static var wood:Int = 0;
    public static var stone:Int = 0;
    public static var iron:Int = 0;
    public static var silk:Int = 0;
    public static var faith:Int = 0;
    public static var knowledge:Int = 0;
    public static var gems:Int = 0;
    public static var techpoint:Int = 0;
}