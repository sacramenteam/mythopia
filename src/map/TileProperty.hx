package map;

import h2d.Font;

/**
 * this class contain various of static that are used for tile
 */

class TileProperty {

    public static var BlockedTileList = [ 13, 14, 15, 16, 37, 38, 39, 40, 41, 42, 43, 44];

    public static function isTileBlocked(tid:Int) {
        if (BlockedTileList.indexOf(tid) != -1 ) {
            return true;
        } else {
            return false;
        }
    }
    

}