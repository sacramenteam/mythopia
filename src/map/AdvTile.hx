package map;

import h2d.filter.Filter;
import h2d.col.Polygon;
import h2d.col.Point;
import h2d.col.PolygonCollider;
import h2d.Interactive;
import h2d.Tile;
import scene.AdvMap;

/**
 * called by Advmap to create each tile and create hitbox over it
 */
class AdvTile {
    /** id of the tile (position on the tileset)**/
    public var tid:Int;
    /** x position on the tilemap in tile**/
    public var tx:Int;
    /** y position on the tilemap in tile**/
    public var ty:Int;
    /** x position on the layer **/
    public var x:Float;
    /** y position on the layer **/
    public var y:Float;
    /** tile hitbox **/
    public var tileCollider:h2d.Interactive;

/**
 * Create a new tile for Advmap and generate a hitbox for it
 * @param x x real position on the layer of the map
 * @param y x real position on the layer of the map
 * @param tid id of the tile (position on the tileset)
 * @param tx x position on the tilemap in tile
 * @param ty y position on the tilemap in tile
 */

public function new(x:Float, y:Float, tid:Int, tx:Int, ty:Int) {

    this.x = x;
    this.y = y;
    this.tid = tid;
    this.tx = tx;
    this.ty = ty;

    var tileShape = new Polygon([
			new Point(   this.x,    this.y +   AdvMap.tilesList[this.tid].height * 0.75), 
			new Point(   this.x +   AdvMap.tilesList[this.tid].width * 0.5,    this.y +   AdvMap.tilesList[this.tid].height * 0.5), 
			new Point(   this.x +   AdvMap.tilesList[this.tid].width,    this.y +   AdvMap.tilesList[this.tid].height * 0.75), 
			new Point(   this.x +   AdvMap.tilesList[this.tid].width * 0.5,    this.y +   AdvMap.tilesList[this.tid].height), 
			
		]);
    tileCollider = new h2d.Interactive( AdvMap.tilesList[tid].width, AdvMap.tilesList[tid].height, scene.AdvMap.layers);
    tileCollider.shape = tileShape.getCollider();
    tileCollider.onClick = function( e : hxd.Event ) {
	    trace(this);
        scene.AdvMap.lastSelectedTile = scene.AdvMap.currentSelectedTile;
        scene.AdvMap.currentSelectedTile = this;
        scene.AdvMapUI.tileInfo.text = "current tile : " + scene.AdvMap.currentSelectedTile.toString() + ", last tile : " + scene.AdvMap.lastSelectedTile.toString() + " ... " + scene.AdvMap.hero.toString();
        if ( (scene.AdvMap.lastSelectedTile.tx == scene.AdvMap.hero.getTx() ) && (scene.AdvMap.lastSelectedTile.ty == scene.AdvMap.hero.getTy()) ) {
            scene.AdvMap.hero.calcPath();
        
        }

    }
    
    
    }
    
    public function  toString():String {
        return "Tile with the ID " + this.tid + ", located at " + this.tx + " x, " + this.ty + " y.";
    }
    
}
 
 