package scene;


import h2d.Scene;
import hxd.Window;
import h2d.Tile;
import h2d.Text;
import h2d.Font;
import h2d.Layers;
//import h2d.filter.Ambient;
//import h2d.filter.Bloom;
import hxd.Key in K;
//import h2d.filter.Filter;
import map.AdvTile;
import map.object.Hero;
import map.object.Team;
import map.TileProperty;
import hxd.res.TiledMap;
import h2d.col.Point;
import scene.gameUI.TextButton;
 
class AdvMap extends Scene
{
    
    // Global variable of the game can be accesed  with Game.(insertVar) in any class

    /**Boolean map of the current map true if the position is walkable or false if the position is obstructed&&&  **/
    public static var collisionMap:Array<Array<Bool>>;
    /** Contains all layer of the map**/
    public static var layers:h2d.Layers; 
    public static var currentSelectedTile:AdvTile;
    public static var lastSelectedTile:AdvTile; 
    /**list that contains every sprite of the game**/
    public static var tilesList:Array<Tile>;
    /** current map width **/
    public static var mw:Int;
    /** current map height **/
    public static var mh:Int;
    public static var teamList:Array<Team>;
    public static var currentTeam:Team;
    public static var hero:Hero;


    
    var lastMousePosition:h2d.col.Point;
    var currentMousePosition:h2d.col.Point;
    
    var center:Point;
    var key : { left : Bool, right : Bool, up : Bool, down : Bool}; // insert all controll here
    var event : hxd.WaitEvent; 
    var speed = 1.; // for real time animation
    
    var zoomLevel:Array<Float> = [
		0.021, 0.031, 0.042, 0.062, 0.083, 0.125, 0.167, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 6, 8, 12, 16, 24, 32, 48
	]; // all level of zoomLevel 

    var zoomLevelPos:Int = 11; // default zoomLevel position on the array zoomLevel ( by defaut position 11 = x1 zoomLevel )
    var disp : h2d.Tile; // for moving water with displacement filter (WIP)
    
    /**
     * generate a new adventure map with the tmx file tranformed to map
     * @param tiledMapData  a tmx file transformed with the function toMap() of hxd.res.TiledMap;
     */


    public function new(tiledMapData:TiledMapData) // argument array for team info
    {   
        super();
       
        lastMousePosition = new Point();
        currentMousePosition = new Point();
        

        // load map data
        var mapData:TiledMapData = tiledMapData;
        //disp = hxd.Res.normalmap.toTile();
        event = new hxd.WaitEvent();
        //pad = hxd.Pad.createDummy();
        //hxd.Pad.wait(function(p) this.pad = p);
       
        // get tile image (tiles.png) from resources
        var tileImage = hxd.Res.load("tileset1.png").toTile();
        // tile size
        var tw:Int = 128; // tile width
        var th:Int = 128;  // tile height
        // map size
        mw = mapData.width;
        mh = mapData.height;
        // A loop to parse tile with the correct dimension
        tilesList = new Array<h2d.Tile>();
        collisionMap = new Array<Array<Bool>>();
        collisionMap = [for (y in 0...mh) [for (x in 0...mw) true]];
        

        for (y in 0...Std.int(tileImage.height / th))
        {
            for (x in 0...Std.int(tileImage.width / tw))
            {
                var t = tileImage.sub(x * tw, y * th, tw, th, 0, 0);
                tilesList.push(t);
            }
            
        }

        currentSelectedTile = new AdvTile(0,0,1,0,0);
        lastSelectedTile = new AdvTile(0,0,1,0,0);
        
        // create h2d.Layers object and add it to 2d scene
        layers = new h2d.Layers(this);
        

        // iterate over all layers
        for (layer in mapData.layers)
        {
            // get layer index
            var layerIndex:Int = mapData.layers.indexOf(layer);
            // create tile group for this layer
            var layerGroup:h2d.TileGroup = new h2d.TileGroup(tilesList[0]);
            // and add it to layers object at specified index
            layers.add(layerGroup, layerIndex);
            // you can also add objects to layer over and under specified objects
            // by using `over()` and `under()` methods
 
            // iterate on x and y
            for (y in 0...mh)
            {
                for (x in 0...mw)
                {
                    // get the tile id at the current position
                    var tid = layer.data[x + y * mw];
                   if (TileProperty.isTileBlocked(tid) ) {
                      collisionMap[y][x] = false;  
                    }
                    if (tid != 0) // skip transparent tiles
                    {
                        // create a new AdvTile with a hitbox for click 
                        var currentTile = new AdvTile((x - y) * tw/2, (x + y) * th/4, tid-1, x, y);
                        // add a tile to layer
                        layerGroup.add( currentTile.x, currentTile.y, tilesList[currentTile.tid]);
                        
                        
                    }
                }
            }
            
            // you can also iterate through all objects on specified layer with `getLayer()` method
            for (object in layers.getLayer(1))
            {
                //object.filter = new h2d.filter.Displacement(disp,4,4);
                //object.alpha = 0.6;
                //object.filter = new h2d.filter.Displacement(disp,4,4);
            }
        }

        var nextTeamButton:TextButton = new TextButton(100, 100, FontFolder.smythe80, "next");
        nextTeamButton.x = (this.width-350)*0.5;
        nextTeamButton.y = (this.height-100)*0.6;
        playButton.interactive.onClick = function( e : hxd.Event ) {
            Game.currentScene = new MapSelect();
            Game.instance.setScene(Game.currentScene);
        }
        
        


        //disp = hxd.Res.normalmap.toTile(); // for water shader
        layers.x = 0;
        layers.y = 0;
        teamList = new Array<Team>(); 
        var team1 = new Team(0, 0xFF0000, "Dragon's Breath", "Feel the pain of the anciant fire!");
        teamList.push(team1);
        var team2 = new Team(1, 0xFF0000, "aqua deluge", "Join the dead fish in the deep");
        teamList.push(team2);
        hero = new Hero(team1,"joseph", "il est fort!!!", 0, 0, tilesList[45], layers);
        
        //hero.filter = new h2d.filter.Displacement(disp,4,4);

        
        
        center = new Point( Window.getInstance().width*0.5,  Window.getInstance().height*0.5 );
        AdvMapUI.init(this);
    
    }

    /**
    * code below is executed each time before a frame is generated
    * @param dt time beetwen the last frame and the current frame
    */

    public function update(dt:Float) {
		
        AdvMapUI.update(dt);

        lastMousePosition = currentMousePosition;
        currentMousePosition = new Point( Window.getInstance().mouseX, Window.getInstance().mouseY );

		#if debug
		if( K.isPressed("R".code) )
			speed = speed == 1 ? 0.1 : 1;	
		#end
		hxd.Timer.tmod *= speed;
		dt *= speed;

		key = {
			left : K.isDown(K.LEFT) || K.isDown("Q".code) || K.isDown("A".code) ,
			right : K.isDown(K.RIGHT) || K.isDown("D".code) ,
			up : K.isDown(K.UP) || K.isDown("Z".code) || K.isDown("W".code) ,
			down : K.isDown(K.DOWN) || K.isDown("S".code)  ,
			
		};
       
        
		if( key.right || (Window.getInstance().mouseX > Window.getInstance().width - 5))
			layers.x -= 10;

        if( key.left || (Window.getInstance().mouseX < 5)) 
			layers.x += 10;

        if( key.up || (Window.getInstance().mouseY < 5)) 
			layers.y += 10;

        if( key.down || (Window.getInstance().mouseY > Window.getInstance().height - 5))
			layers.y -= 10; 

        if (K.isPressed(K.MOUSE_WHEEL_UP) && K.isDown(K.CTRL)) {
			if (zoomLevelPos < zoomLevel.length - 1) {
                var oldZoom = zoomLevel[zoomLevelPos];
				zoomLevelPos++;
                var difZoom = zoomLevel[zoomLevelPos] - oldZoom;
                var vector = currentMousePosition.sub(center);
                layers.x -= vector.x * difZoom;
                layers.y -= vector.y * difZoom;
                }
		}
        
		if (K.isPressed(K.MOUSE_WHEEL_DOWN) && K.isDown(K.CTRL)) {
			if (zoomLevelPos > 0) {
                var oldZoom = zoomLevel[zoomLevelPos];
				zoomLevelPos--;
                var difZoom = zoomLevel[zoomLevelPos] - oldZoom;
                var vector = currentMousePosition.sub(center);
                layers.x += vector.x * difZoom;
                layers.y += vector.y * difZoom;
                }
        }

         if (K.isDown(K.MOUSE_LEFT) && K.isDown(K.CTRL)) {
            var vector = currentMousePosition.sub(lastMousePosition);
            layers.x += vector.x;
            layers.y += vector.y;
        }


        layers.setScale(zoomLevel[zoomLevelPos]);

        //disp.scrollDiscrete(1.2 * dt, 2.4 * dt);// for water shader
       

    }

    /**
     * code below is executed each time the window is resized ( don't work on js )
     */

    public function onResize() {
        center = new Point( Window.getInstance().width*0.5,  Window.getInstance().height*0.5 );
        AdvMapUI.onResize();
    }



}