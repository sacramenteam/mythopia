package scene;


import h2d.Scene;
import hxd.Window;
import h2d.Tile;
import h2d.Text;
import h2d.Font;
import h2d.Layers;
import hxd.Key in K;
import scene.gameUI.TextButton;
 
class AdvMapUI 
{
    public static var backButton:TextButton;
    public static var font:Font;// Text default font
    public static var fps:h2d.Text; // label that contains fps counter
    public static var fpsint:Float; // current fps
    /**Text UI that contain the info of selected tile ( debug purpose only ) **/
    public static var tileInfo:Text;
    public static var command:Text;

    public static function init(parent:Scene) {

        font = hxd.res.DefaultFont.get();
        tileInfo = new h2d.Text(font);
        tileInfo.textColor = 0xfefefe;
        tileInfo.x = 250;
        tileInfo.y = 35;
        tileInfo.text = "Tile selected here.";
        parent.addChild(tileInfo);

        command = new h2d.Text(font);
        command.textColor = 0xfefefe;
        command.x = 250;
        command.y = 5;
        command.text = "zqsd/wasd or arrow to move the camera, ctrl + mouse wheel to zoomLevel, click on the hero then click on your destination";
        parent.addChild(command);

        fps = new h2d.Text(font);
        fps.textColor = 0xfefefe;
        fps.x = 250;
        fps.y = 55;
        fpsint = hxd.Timer.fps();
        fps.text = Std.string(Math.round(fpsint));
        parent.addChild(fps);

        backButton = new TextButton(90,90, FontFolder.smythe80, "<");
        backButton.x = 3;
        backButton.y = 3;
        backButton.label.y -= 10;
        backButton.interactive.onClick = function( e : hxd.Event ) {
            parent = new Menu();
            Game.instance.setScene(parent);
        }
        parent.addChild(backButton);
        trace(parent);
    }

    public static function update(dt:Float) {

        fpsint = hxd.Timer.fps();
        fps.text = Std.string(Math.round(fpsint));


    }

    public static function onResize() {



    }

}