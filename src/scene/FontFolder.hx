package scene;

import h2d.Font;

/**
 * this class is init at the start of the game, it contains all font of the game.
 * Font can be accesed like this FontFolder.smythe??? where ??? is the size of the font
 */

class FontFolder {

    /** Smythe font of size 80 pixel height **/
    public static var smythe80:Font;
    /** Smythe font of size 250 pixel height **/
    public static var smythe250:Font;
    
    /**
     * called at the start of the game to initiate all font and adjust their size
     */

    public static function init() {
        smythe80 = hxd.Res.MainMenu.Smythe80.toFont();
        smythe80.resizeTo(80);
        smythe250 = hxd.Res.MainMenu.Smythe250.toFont();
        smythe250.resizeTo(250);
    }

}